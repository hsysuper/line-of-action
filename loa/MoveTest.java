
package loa;

import static org.junit.Assert.*;

import org.junit.Test;

/** A list of tests for Move.java.
 *
 * @author Siyuan He
 *
 */
public class MoveTest {

    /** A test for equality of two moves. */
    @Test
    public void equalTest() {
        Move move1 = Move.create(1, 2, 3, 4);
        Move move2 = Move.create(1, 2, 3, 4);
        assertEquals("Two moves with same parameters should be equal:", true,
                move1.equals(move2));
    }

    /** A test for hashing moves. */
    @Test
    public void hashTest() {
        Move move1 = Move.create(1, 2, 3, 4);
        Move move2 = Move.create(1, 2, 3, 4);
        assertEquals(
                "Two moves wish same paramter should have same hashcode",
                true, move1.hashCode() == move2.hashCode());
        Move move3 = Move.create(4, 6, 7, 8);
        assertEquals("Two moves with different parameter should have"
                + "different hashcode:", false,
                move1.hashCode() == move3.hashCode());
    }

    /** A test for the toString() method in move. */
    @Test
    public void toStringTest() {
        Move move1 = Move.create(1, 2, 3, 4);
        assertEquals("The string representation should be:", "a2-c4",
                move1.toString());
    }

    /** A test of the direction method in move. */
    @Test
    public void directionTest() {
        Move wmove = Move.create(5, 5, 3, 5);
        Move nwmove = Move.create(5, 5, 3, 7);
        assertEquals("The move direction is wrong", 5, wmove.direction());
        assertEquals("The move direction is wrong", 4, nwmove.direction());
    }

}
