package loa;

/** A move in Lines of Action.
 *  @author Siyuan He*/
class Move {

    /* Implementation note: We create moves by means of static "factory
     * methods" all named create, which in turn use the single (private)
     * constructor.  Factory methods have certain advantages over constructors:
     * they allow you to produce results having an arbitrary subtype of Move,
     * and they don't require that you produce a new object each time.  This
     * second advantage is useful when you are trying to speed up the creation
     * of Moves for use in automated searching for moves.  You can (if you
     * want) create just one instance of the Move representing 1-5, for example
     * and return it whenever that move is requested. */

    /** Return a move of the piece at COLUMN0, ROW0 to COLUMN1, ROW1. */
    static Move create(int column0, int row0, int column1, int row1) {
        return new Move(column0, row0, column1, row1);
    }

    /** A new Move of the piece at COL0, ROW0 to COL1, ROW1. */
    private Move(int col0, int row0, int col1, int row1) {
        _col0 = col0;
        _row0 = row0;
        _col1 = col1;
        _row1 = row1;

    }

    /** Return the column at which this move starts, as an index in 1--8. */
    int getCol0() {
        return _col0;
    }

    /** Return the row at which this move starts, as an index in 1--8. */
    int getRow0() {
        return _row0;
    }

    /** Return the column at which this move ends, as an index in 1--8. */
    int getCol1() {
        return _col1;
    }

    /** Return the row at which this move ends, as an index in 1--8. */
    int getRow1() {
        return _row1;
    }

    /** Return the length of this move (number of squares moved). */
    int length() {
        return Math.max(Math.abs(_row1 - _row0), Math.abs(_col1 - _col0));
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Move)) {
            return false;
        }
        Move move2 = (Move) obj;
        return this.getCol0() == move2.getCol0()
                && this.getCol1() == move2.getCol1()
                && this.getRow0() == move2.getRow0()
                && this.getRow1() == move2.getRow1();
    }

    @Override
    public int hashCode() {
        int hash = THOUSAND * this.getCol0() + HUNDRED * this.getRow0()
                + 10 * this.getCol1() + this.getRow1();
        return new Integer(hash).hashCode();
    }

    @Override
    public String toString() {
        String startC = String.valueOf((char) (this.getCol0() + ASCII));
        String endC = String.valueOf((char) (this.getCol1() + ASCII));
        return new String(startC + (this.getRow0()) + "-" + endC
                + (this.getRow1()));
    }

    /** Return the direction of the move as an int.
     *  0 - Origin
     *  1 - E, 2 - NE, 3 - N, 4 - NW, 5 - W
     *  6 - SW, 7 - S, 8 - SE
     */
    int direction() {
        int hor = _col1 - _col0;
        int ver = _row1 - _row0;
        if (ver == 0) {
            return hor > 0 ? 1 : 5;
        }
        if (hor == 0) {
            return ver > 0 ? 3 : 7;
        }
        if (ver > 0 && hor > 0) {
            return 2;
        }
        if (ver > 0 && hor < 0) {
            return 4;
        }
        if (ver < 0 && hor < 0) {
            return 6;
        }
        if (ver < 0 && hor > 0) {
            return 8;
        }
        return 0;
    }

    /** Return the weighted value of current move. */
    double value() {
        return _value;
    }

    /** Set the weighted value of current move to VALUE. */
    void setValue(double value) {
        _value = value;
    }

    /** Store the current value of the move. */
    private double _value;

    /** Column and row numbers of starting and ending points. */
    private int _col0, _row0, _col1, _row1;

    /** A number for 1000. */
    private static final int THOUSAND = 1000;

    /** A number for 100. */
    private static final int HUNDRED = 100;

    /** The ascii code just before 'a'. */
    private static final int ASCII = 96;

}
