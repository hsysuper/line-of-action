package loa;

import java.awt.Color;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Rectangle2D;

import ucb.gui.Pad;


class Board2D extends Pad{

    Board2D(Board board, double diameter, Point initiallocation,
            Color color1, Color color2) {
        _boardsize = board.size();
        _squaresize = diameter;
        _location = initiallocation;
        _color1 = color1;
        _color2 = color2;
        super.setMinimumSize((int) diameter * (_boardsize - 1), (int) diameter * (_boardsize - 1));
    }

    public void render(Graphics2D g){
        boolean one = true;
        for (double y = y(); y >= y - d() * (_boardsize - 1); y -= d()) {
            for (double x = x(); x <= x + d() * (_boardsize - 1); x += d()) {
                Rectangle2D square = new Rectangle2D.Double(x, y, d(), d());
                g.setPaint(one ? _color1 : _color2);
                g.draw(square);
                g.fill(square);
                one = !one;
            }
        }
    }

    double x() {
        return _location.x;
    }

    double y() {
        return _location.y;
    }

    double d() {
        return _squaresize;
    }
    
    private int _boardsize;
    private double _squaresize;
    private Point _location;
    private Color _color1;
    private Color _color2;

}
