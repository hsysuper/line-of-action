
package loa;

/** A Player that prompts for moves and reads them from its Game.
 *
 * @author Siyuan He */
class HumanPlayer extends Player {

    /** A HumanPlayer that plays the SIDE pieces in GAME as index N. It uses
     * GAME.getMove() as a source of moves. */
    HumanPlayer(Side side, Game game, int N) {
        super(side, game, N);
    }

    @Override
    Move makeMove() {
        String mvstr = "";
        do {
            mvstr = this.getGame().getMove(this);
        } while (mvstr.equals("GO!"));
        String sq0 = mvstr.substring(0, 2);
        String sq1 = mvstr.substring(3, 5);
        Move move =
                Move.create(Board.col(sq0), Board.row(sq0), Board.col(sq1),
                        Board.row(sq1));
        return move;
    }

    @Override
    boolean isAI() {
        return false;
    }

}
