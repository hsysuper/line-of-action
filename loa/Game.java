
package loa;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.nio.CharBuffer;
import java.util.Random;
import java.util.Scanner;
import static loa.Side.*;

import ucb.util.Stopwatch;

/** Represents one game of Lines of Action.
 *  @author Siyuan He*/
class Game {

    /** A new Game between NUMHUMAN humans and 2-NUMHUMAN AIs.  SIDE0
     *  indicates which side the first player (known as ``you'') is
     *  playing.  SEED is a random seed for random-number generation.
     *  TIME is the time limit each side has to make its moves (in seconds).
     */
    Game(int numHuman, Side side0, long seed, int time, boolean display) {
        _timer1 = new Stopwatch();
        _timer2 = new Stopwatch();
        _randomSource = new Random(seed);
        _tlimit = time * SMS;
        _board = new MutableBoard();
        _ai = false;
        _nai = 2 - numHuman;
        if (numHuman == 2) {
            _player1 = new HumanPlayer(BLACK, this, 1);
            _player2 = new HumanPlayer(WHITE, this, 2);
        } else if (numHuman == 1) {
            if (side0 == BLACK) {
                _player1 = new HumanPlayer(BLACK, this, 1);
                _player2 = new MachinePlayer(WHITE, this, 2);
            } else {
                _player1 = new MachinePlayer(BLACK, this, 1);
                _player2 = new HumanPlayer(WHITE, this, 2);
            }
        } else {
            _player1 = new MachinePlayer(BLACK, this, 1);
            _player2 = new MachinePlayer(WHITE, this, 2);
        }
        _display = display;
        if (display) {
            try {
                _output = new PipedOutputStream();
                _input = new PipedInputStream();
                _input.connect(_output);
                _gui = new LoaGui("Line of Action", true, this);
                _inp = new Scanner(_input);
            } catch (IOException e) {
                System.err.println("Error: " + e.getMessage());
                System.exit(1);
            }
        } else {
            _inp = new Scanner(System.in);
        }
    }

    /** Return the current board. */
    Board getBoard() {
        return _board;
    }

    void writeToBuffer(String word) {
        try {
            _output.write(word.getBytes(), 0, word.getBytes().length);
            _output.flush();
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
            System.exit(1);
        }
    }

    /** Return a move for PLAYER from the terminal.  Processes any
     *  intervening commands as well. */
    String getMove(Player player) {
        if (player.isAI() && _ai) {
            return "GO!";
        }
        while (true) {
            System.out.printf("%s Player (%d seconds left)> ",
                    _board.turn() == BLACK ? "Black" : "White",
                    this.timeRemaining(_board.turn()));
            System.out.flush();
            if (!_inp.hasNextLine()) {
                System.exit(0);
            }
            String line = _inp.nextLine();
            String[] commands = line.split("\\s+|\\z|\\n");
            if (commands[0].equals("s")) {
                System.out.println("===");
                System.out.printf(this._board.toString());
                System.out.println("===");
            } else if (commands[0].equals("p")) {
                if (_nai >= 1) {
                    if (!_ai) {
                        _ai = true;
                        if (player.isAI()) {
                            (player.getIndex() == 1
                                    ? _timer1 : _timer2).start();
                            return "GO!";
                        }
                    }
                } else {
                    System.out.println("There is no AI(s) in this game.");
                }
            } else if (commands[0].equals("q")) {
                System.exit(0);
            } else if (commands[0].equals("r")) {
                if (_board.turn() == WHITE && _board.movesMade() <= 1) {
                    System.out.println("No move to retract.");
                } else if (_board.turn() == BLACK && _board.movesMade() <= 0) {
                    System.out.println("No move to retract.");
                } else {
                    _board.retract();
                    _board.retract();
                    System.out.println(
                            (_board.turn() == BLACK ? "Black" : "White")
                            + " player retracts 1 step back.");
                }
            } else if (commands[0].matches("#.*")) {
                continue;
            } else if (commands[0].matches("[a-z][1-8]-[a-z][1-8]")) {
                return commands[0];
            } else if (commands[0] == null) {
                continue;
            } else {
                throw new LoaException("Error: Wrong Command!");
            }
        }
    }

    /** Play this game, printing any transcript and other results. */
    public void play() {
        while (true) {
            Side current;
            while (true) {
                try {
                    if (!_player1.isAI() || _ai) {
                        _timer1.start();
                    }
                    Move move1 = _player1.makeMove();
                    if (!_player1.isAI() || _ai) {
                        _timer1.stop();
                    }
                    if (_timer1.getAccum() > _tlimit) {
                        System.out.println("Black Player is out of time!");
                        System.out.println("White wins.");
                        System.exit(0);
                    }
                    _board.makeMove(move1);
                    if (_display) {
                        _gui.makeMove(move1);
                    }
                    break;
                } catch (LoaException e) {
                    System.out.println(e.getMessage());
                }
            }
            current = _board.gameOver();
            if (current != null) {
                String msg = (current == WHITE ? "White wins." : "Black wins.");
                if (_display) {
                    _gui.showMessage(msg, "Congratulations!", "information");
                } else {
                    System.out.println(msg);
                }
                break;
            }
            while (true) {
                try {
                    if (!_player2.isAI() || _ai) {
                        _timer2.start();
                    }
                    Move move2 = _player2.makeMove();
                    if (!_player2.isAI() || _ai) {
                        _timer2.stop();
                    }
                    if (_timer2.getAccum() > _tlimit) {
                        System.out.println("White Player is out of time!");
                        System.out.println("Black wins.");
                        System.exit(0);
                    }
                    _board.makeMove(move2);
                    if (_display) {
                        _gui.makeMove(move2);
                    }
                    break;
                } catch (LoaException e) {
                    System.out.println(e.getMessage());
                }
            }
            current = _board.gameOver();
            if (current != null) {
                String msg = (current == BLACK ? "Black wins." : "White wins.");
                if (_display) {
                    _gui.showMessage(msg, "Congratulations!", "information");
                } else {
                    System.out.println(msg);
                }
            }
        }
    }

    LoaGui getGUI() {
        return _gui;
    }

    boolean display() {
        return _display;
    }

    /** Return time remaining for SIDE (in seconds). */
    int timeRemaining(Side side) {
        return side == BLACK ? (int) ((_tlimit - _timer1.getAccum()) / SMS)
                : (int) ((_tlimit - _timer2.getAccum()) / SMS);
    }

    /** Return the random number generator for this game. */
    Random getRandomSource() {
        return _randomSource;
    }

    /** The official game board. */
    private MutableBoard _board;

    /** A source of random numbers, primed to deliver the same sequence in
     *  any Game with the same seed value. */
    private Random _randomSource;

    /** Player 1. */
    private Player _player1;

    /** Player 1. */
    private Player _player2;

    /** Time limit for each player. */
    private double _tlimit;

    /** Timer for Player 1. */
    private Stopwatch _timer1;

    /** Timer for Player 2. */
    private Stopwatch _timer2;

    /** Scanner for inputs. */
    private Scanner _inp;

    /** An indicator for whether AI(s) has been started. */
    private boolean _ai;

    /** The number of AI(s) playing this game. */
    private int _nai;

    private boolean _display;
    
    private LoaGui _gui;

    private PipedInputStream _input;

    private PipedOutputStream _output;

    /** Conversion parameter between second and miliseconds. */
    private static final double SMS = 1000;

}
