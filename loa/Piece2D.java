package loa;

import java.awt.Color;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;

import static loa.Side.*;
import ucb.gui.Pad;


class Piece2D extends Pad{

    Piece2D(int diameter, Color bcolor, Side side, LoaGui gui, int c, int r) {
        super();
        super.setSize(diameter, diameter);
        _gui = gui;
        _diameter = diameter;
        _bcolor = bcolor;
        _side = side;
        _fcolor = sideColor(side);
        _c = c;
        _r = r;
        super.setPreferredSize(diameter, diameter);
        super.setMouseHandler("click", "senseClick");
    }

    void senseClick(MouseEvent event) {
        if (event.getButton() == MouseEvent.BUTTON1) {
            //this.dim();
            Move move = _gui.recordMove(this);
            if (move != null) {
                //this.back();
                this._gui.reportCommand(move.toString());
            }
        }
    }

    void dim() {
        _fcolor = _fcolor.brighter();
        _buffer = _fcolor;
        this.repaint();
    }
    
    void back() {
        _fcolor = _buffer;
        this.repaint();
    }

    private Color _buffer;

    Color sideColor(Side side) {
        if (side == BLACK) {
            return Color.BLACK;
        } else if (side == WHITE) {
            return Color.WHITE;
        } else {
            return null;
        }
     }

    void setSide(Side side) {
        _side = side;
        _fcolor = sideColor(side);
        this.repaint();
    }

    Side getSide() {
        return _side;
    }

    int getRow() {
        return _r;
    }

    int getCol() {
        return _c;
    }

    @Override
    public void paintComponent(Graphics2D g) {
        int w = this.getHeight();
        int h = this.getHeight();
        g.setColor(_bcolor);
        g.fillRect(0, 0, w, h);
        g.setColor(Color.BLACK);
        g.drawRect(0, 0, w-1, h-1);
        if (_fcolor != null) {
            Ellipse2D circle = new Ellipse2D.Double(0, 0, d(), d());
            g.draw(circle);
            g.setPaint(_fcolor);
            g.fill(circle);
        }
    }

    double d() {
        return _diameter;
    }

    private double _diameter;
    private Color _bcolor;
    private Color _fcolor;
    private Side _side;
    private LoaGui _gui;
    private int _c, _r;
}
