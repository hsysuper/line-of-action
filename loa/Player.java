
package loa;

/** Represents a player. Extensions of this class do the actual playing.
 *
 * @author Siyuan He */
public abstract class Player {

    /** A player that plays the SIDE pieces in GAME as Player N. */
    Player(Side side, Game game, int N) {
        _side = side;
        _game = game;
        _index = N;
    }

    /** Return my next move from the current position in getBoard(), assuming
     * that side() == getBoard.turn(). */
    abstract Move makeMove();

    /** Return which side I'm playing. */
    Side side() {
        return _side;
    }

    /** Return the board I am using. */
    Board getBoard() {
        return _game.getBoard();
    }

    /** Return the game I am playing. */
    Game getGame() {
        return _game;
    }

    /** Return the index of this player. */
    int getIndex() {
        return _index;
    }

    /** Return true iff the player is a machine player. */
    abstract boolean isAI();

    /** This player's side. */
    private final Side _side;

    /** The game this player is part of. */
    private Game _game;

    /** The index of this player in game. */
    private int _index;

}
