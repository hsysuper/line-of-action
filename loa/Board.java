
package loa;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static loa.Piece.*;
import static loa.Side.*;

/** Represents the state of a game of Lines of Action. A Board is immutable.
 * Its MutableBoard subclass allows moves to be made.
 *
 * @author Siyuan He */
class Board {

    /** A Board whose initial contents are taken from INITIALCONTENTS and in
     * which it is PLAYER's move. The resulting Board has get(col, row) ==
     * INITIALCONTENTS[row-1][col-1] Assumes that PLAYER is not null and
     * INITIALCONTENTS is 8x8. */
    Board(Piece[][] initialContents, Side player) {
        assert player != null && initialContents.length == 8;
        data = new Piece[8][8];
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data.length; j++) {
                data[i][j] = initialContents[i][j];
            }
        }
        _side = player;
        moves = new ArrayList<Move>();
    }

    /** A new board in the standard initial position. */
    Board() {
        this(INITIAL_PIECES, BLACK);
    }

    /** A Board whose initial contents and state are copied from BOARD. */
    Board(Board board) {
        data = new Piece[8][8];
        for (int i = 1; i <= 8; i++) {
            for (int j = 1; j <= 8; j++) {
                data[i - 1][j - 1] = board.get(j, i);
            }
        }
        moves = new ArrayList<Move>();
        for (int j = 0; j < board.movesMade(); j++) {
            moves.add(board.getMove(j));
        }
        _side = board.turn();
    }

    /** Return the contents of column C, row R, where 1 <= C,R <= 8, where
     * column 1 corresponds to column 'a' in the standard notation. */
    Piece get(int c, int r) {
        assert 1 <= c && c <= 8 && 1 <= r && r <= 8;
        return data[r - 1][c - 1];
    }

    /** Return the contents of the square SQ. SQ must be the standard printed
     * designation of a square (having the form cr, where c is a letter from
     * a-h and r is a digit from 1-8). */
    Piece get(String sq) {
        return get(col(sq), row(sq));
    }

    /** Return the column number (a value in the range 1-8) for SQ. SQ is as
     * for {@link get(String)}. */
    static int col(String sq) {
        return sq.charAt(0) - ASCII;
    }

    /** Return the row number (a value in the range 1-8) for SQ. SQ is as for
     * {@link get(String)}. */
    static int row(String sq) {
        return Integer.valueOf(String.valueOf(sq.charAt(1)));
    }

    /** Return the Side that is currently next to move. */
    Side turn() {
        return _side;
    }

    /** Return true iff MOVE is legal for the player currently on move. */
    boolean isLegal(Move move) {
        int dir = move.direction();
        if (!inRange(move)) {
            if (Reporter.getMessageLevel() > 0) {
                System.out.println(1);
            }
            return false;
        }
        if (!inEigthDirection(move)) {
            return false;
        }
        List<Piece> line = getLoa(move);
        int length = move.length();
        Piece self = line.get(0);
        if (!onMySide(self)) {
            if (Reporter.getMessageLevel() > 0) {
                System.out.println(self.side().toString());
                System.out.println("Board side is " + _side.toString());
                System.out.println(4);
            }
            return false;
        }
        int count = numPiece(line);
        if (count != length) {
            if (Reporter.getMessageLevel() > 0) {
                for (Piece item : line) {
                    System.out.println(item.textName());
                }
                System.out.println(5);
            }
            return false;
        }
        for (int i = 1; i < length - 1; i++) {
            if (!onMySide(line.get(i)) && line.get(i).side() != null) {
                if (Reporter.getMessageLevel() > 0) {
                    System.out.println(i);
                    System.out.println(6);
                }
                return false;
            }
        }
        if (onMySide(line.get(length))) {
            if (Reporter.getMessageLevel() > 0) {
                System.out.println(7);
                System.out.println("Direction is " + dir);
                for (Piece item : line) {
                    System.out.println(item.textName());
                }
            }
            return false;
        }
        return true;
    }

    /** Return true iff MOVE is in range of this board. */
    boolean inRange(Move move) {
        int size = data.length;
        return move.getCol0() <= size && move.getCol0() > 0
                && move.getCol1() <= size && move.getCol1() > 0
                && move.getRow0() <= size && move.getRow0() > 0
                && move.getRow1() <= size && move.getRow1() > 0;
    }

    /** Return true iff MOVE is in the 8 legal directions. */
    boolean inEigthDirection(Move move) {
        int dir = move.direction();
        if (dir == 0) {
            if (Reporter.getMessageLevel() > 0) {
                System.out.println(2);
            }
            return false;
        } else if (dir != 1 && dir != 3 && dir != 5 && dir != 7) {
            if (Math.abs(move.getCol1() - move.getCol0()) != Math.abs(move
                    .getRow1() - move.getRow0())) {
                if (Reporter.getMessageLevel() > 0) {
                    System.out.println(3);
                }
                return false;
            }
        }
        return true;
    }

    /** Return true iff PIECE in on current player's side. */
    boolean onMySide(Piece piece) {
        return piece.side() == this.turn();
    }

    /** Get the line of action of MOVE, return the list of Pieces, including
     * empty ones on this line, starting with origin piece.
     */
    List<Piece> getLoa(Move move) {
        int dir = move.direction();
        int c = move.getCol0();
        int r = move.getRow0();
        return getLoa(c, r, dir);
    }

    /** Return the list of Pieces on the line of action starting from (C, R).
     *  Point towards DIR direction,
     */
    List<Piece> getLoa(int c, int r, int dir) {
        int c0 = c;
        int r0 = r;
        int size = data.length;
        List<Piece> result = new ArrayList<Piece>();
        if (dir == 1) {
            for (int i = c; i <= size; i++) {
                result.add(this.get(i, r));
            }
            for (int i = c - 1; i >= 1; i--) {
                result.add(this.get(i, r));
            }
        } else if (dir == 5) {
            for (int i = c; i >= 1; i--) {
                result.add(this.get(i, r));
            }
            for (int i = c + 1; i <= size; i++) {
                result.add(this.get(i, r));
            }
        } else if (dir == 3) {
            for (int i = r; i <= size; i++) {
                result.add(this.get(c, i));
            }
            for (int i = r - 1; i >= 1; i--) {
                result.add(this.get(c, i));
            }
        } else if (dir == 7) {
            for (int i = r; i >= 1; i--) {
                result.add(this.get(c, i));
            }
            for (int i = r + 1; i <= size; i++) {
                result.add(this.get(c, i));
            }
        }
        return getLoaHelper(dir, size, c0, r0, result);
    }

    /** A helper function to getLoa. Return the return the list of Pieces,
     * including empty ones on the line of action indicated by DIR, starting
     * on location C0, R0, with board SIZE and previous RESULT. */
    List<Piece> getLoaHelper(int dir, int size, int c0, int r0,
            List<Piece> result) {
        int c = c0;
        int r = r0;
        if (dir == 2) {
            while (c <= size && r <= size) {
                result.add(this.get(c, r));
                c++;
                r++;
            }
            c = c0 - 1;
            r = r0 - 1;
            while (c >= 1 && r >= 1) {
                result.add(this.get(c, r));
                c--;
                r--;
            }
        } else if (dir == 6) {
            while (c >= 1 && r >= 1) {
                result.add(this.get(c, r));
                c--;
                r--;
            }
            c = c0 + 1;
            r = r0 + 1;
            while (c <= size && r <= size) {
                result.add(this.get(c, r));
                c++;
                r++;
            }
        } else if (dir == 4) {
            while (c >= 1 && r <= size) {
                result.add(this.get(c, r));
                c--;
                r++;
            }
            c = c0 + 1;
            r = r0 - 1;
            while (c <= size && r >= 1) {
                result.add(this.get(c, r));
                c++;
                r--;
            }
        } else if (dir == 8) {
            while (c <= size && r >= 1) {
                result.add(this.get(c, r));
                c++;
                r--;
            }
            c = c0 - 1;
            r = r0 + 1;
            while (c >= 1 && r <= size) {
                result.add(this.get(c, r));
                c--;
                r++;
            }
        }
        return result;
    }

    /** Return a sequence of all legal from this position. */
    Iterator<Move> legalMoves() {
        return legalMovesList().iterator();
    }

    /** Return a list of all legal from this position. */
    List<Move> legalMovesList() {
        List<Move> result = new ArrayList<Move>();
        for (int i = 1; i <= data.length; i++) {
            for (int j = 1; j <= data.length; j++) {
                result.addAll(legalMovesFrom(i, j, this.turn()));
            }
        }
        return result;
    }

    /** Return a list of all legal for PLAYER from row R and column C,
     * with R and C being non-indexial locations on board. */
    List<Move> legalMovesFrom(int c, int r, Side player) {
        List<Move> result = new ArrayList<Move>();
        Move move = null;
        if (this.get(c, r).side() != player) {
            return result;
        }
        for (int dir : DIRECTIONS) {
            move = legalMoveOnDir(c, r, dir);
            if (move != null) {
                result.add(move);
            }
        }
        return result;
    }

    /** Return the legal move for Piece on (C, R) towards direction DIR.
     *  Assume Piece (C, R) is player's piece.
     */
    Move legalMoveOnDir(int c, int r, int dir) {
        List<Piece> line = getLoa(c, r, dir);
        int num = numPiece(line);
        if (num >= line.size()) {
            return null;
        }
        Piece origin = line.get(0);
        Piece target = line.get(num);
        if (origin.side() == target.side()) {
            return null;
        }
        for (Piece item : line.subList(1, num)) {
            if (item.side() == origin.side().opponent()) {
                return null;
            }
        }
        Move move = null;
        if (dir == 1 || dir == 5) {
            move = Move.create(c, r, c + (dir == 1 ? num : -num), r);
        } else if (dir == 3 || dir == 7) {
            move = Move.create(c, r, c, r + (dir == 3 ? num : -num));
        } else if (dir == 2 || dir == 6) {
            num = (dir == 2 ? num : -num);
            move = Move.create(c, r, c + num, r + num);
        } else if (dir == 4 || dir == 8) {
            num = (dir == 4 ? num : -num);
            move = Move.create(c, r, c - num, r + num);
        }
        if (inRange(move) || move == null) {
            return move;
        } else {
            return null;
        }
    }

    /** Return the number of non-null pieces on LINE. */
    int numPiece(Piece[] line) {
        int count = 0;
        for (Piece item : line) {
            if (item.side() != null) {
                count++;
            }
        }
        return count;
    }

    /** Return the number of non-null pieces on LINE. */
    int numPiece(List<Piece> line) {
        int count = 0;
        for (Piece item : line) {
            if (item.side() != null) {
                count++;
            }
        }
        return count;
    }

    /** Return the side of player whose pieces are contiguous.
     *  BLACK for black, WHITE for white, BOTH for both, NULL for non of them.
     */
    Side gameOver() {
        if (piecesContiguous(BLACK)) {
            if (piecesContiguous(WHITE)) {
                return BOTH;
            } else {
                return BLACK;
            }
        } else if (piecesContiguous(WHITE)) {
            return WHITE;
        } else {
            return null;
        }
    }

    /** Return true iff PLAYER's pieces are continguous. */
    boolean piecesContiguous(Side player) {
        return getPiecesGraph(player).size() == 1;
    }

    /** Return the list of all non connecting location of PLAYER's Pieces. */
    List<List<Point>> getPiecesGraph(Side player) {
        List<List<Point>> pool = new ArrayList<List<Point>>();
        for (int i = 1; i <= data.length; i++) {
            for (int j = 1; j <= data.length; j++) {
                Point here = new Point(i, j);
                boolean add = true;
                for (List<Point> L : pool) {
                    if (L.contains(here)) {
                        add = false;
                    }
                }
                if (add) {
                    List<Point> temp =
                            countPiece(i, j, player, new ArrayList<Point>());
                    if (!temp.isEmpty()) {
                        pool.add(temp);
                    }
                }
            }
        }
        return pool;
    }

    /** Return a list containing all locations of Pieces that are on the same
     * contiguous unit as the Piece on column C and row R whose side is PLAYER
     * and all results are stored into a list RESULT.
     */
    List<Point> countPiece(int c, int r, Side player, List<Point> result) {
        if (c < 1 || c > data.length || r < 1 || r > data.length) {
            return result;
        }
        if ((!result.contains(new Point(c, r)))
                && (this.get(c, r).side() == player)) {
            result.add(new Point(c, r));
            countPiece(c + 1, r, player, result);
            countPiece(c + 1, r + 1, player, result);
            countPiece(c, r + 1, player, result);
            countPiece(c - 1, r + 1, player, result);
            countPiece(c - 1, r, player, result);
            countPiece(c - 1, r - 1, player, result);
            countPiece(c, r - 1, player, result);
            countPiece(c + 1, r - 1, player, result);
        }
        return result;
    }

    /** Return the total number of moves that have been made (and not
     * retracted). Each valid call to makeMove with a normal move increases
     * this number by 1. */
    int movesMade() {
        return moves.size();
    }

    /** Returns move #K used to reach the current position, where K >= 0
     *  and K < movesMade(). Does not include retracted moves. */
    Move getMove(int k) {
        return moves.get(k);
    }

    /** Return the size of this board. */
    int size() {
        return data.length;
    }

    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < data.length; i++) {
            result = lineToStr(data[i]) + result;
            if (Reporter.getMessageLevel() > 1) {
                result = (i + 1) + " " + result;
            }
        }
        if (Reporter.getMessageLevel() > 1) {
            result += "    a b c d e f g h\n";
        }
        result += ("Next move: " + _side.toString() + "\n");
        result += ("Moves: " + this.movesMade() + "\n");
        return result;
    }

    /** Return the string representation of a LINE of Pieces. */
    String lineToStr(Piece[] line) {
        String result = "  ";
        for (int i = 0; i < line.length - 1; i++) {
            result += line[i].textName();
            result += " ";
        }
        result += line[line.length - 1].textName();
        result += "\n";
        return result;
    }

    /** A 2-D array used to store the board data. */
    protected Piece[][] data;

    /** A pointer for current player. */
    protected Side _side;

    /** A list storing all moves made on this board. */
    protected List<Move> moves;

    /** The standard initial configuration for Lines of Action. */
    static final Piece[][] INITIAL_PIECES = {
        { EMP, BP, BP, BP, BP, BP, BP, EMP },
        { WP, EMP, EMP, EMP, EMP, EMP, EMP, WP },
        { WP, EMP, EMP, EMP, EMP, EMP, EMP, WP },
        { WP, EMP, EMP, EMP, EMP, EMP, EMP, WP },
        { WP, EMP, EMP, EMP, EMP, EMP, EMP, WP },
        { WP, EMP, EMP, EMP, EMP, EMP, EMP, WP },
        { WP, EMP, EMP, EMP, EMP, EMP, EMP, WP },
        { EMP, BP, BP, BP, BP, BP, BP, EMP } };

    /** The ascii code just before 'a'. */
    private static final int ASCII = 96;

    /** 8 directions. */
    private static final int[] DIRECTIONS = {1, 2, 3, 4, 5, 6, 7, 8};

}
