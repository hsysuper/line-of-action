package loa;

/** An unchecked exception that Represents any kind of user error in the
 *  input of a Line of Action game.
 *  @author Siyuan He
 */
class LoaException extends RuntimeException {

    /** A LoaException with no message. */
    LoaException() {
    }

    /** A LoaException for which .getMessage() is MSG. */
    LoaException(String msg) {
        super(msg);
    }

}
