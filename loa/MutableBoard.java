
package loa;

import java.util.ArrayList;
import java.util.List;

/** Represents the state of a game of Lines of Action, and allows making moves.
 *
 * @author Siyuan He */
class MutableBoard extends Board {

    /** A MutableBoard whose initial contents are taken from INITIALCONTENTS
     * and in which it is PLAYER's move. The resulting Board has
     * get(col, row) == INITIALCONTENTS[row-1][col-1] Assumes that PLAYER is
     * not null and INITIALCONTENTS is 8x8. */
    MutableBoard(Piece[][] initialContents, Side player) {
        super(initialContents, player);
        removed = new ArrayList<Piece>();
    }

    /** A new board in the standard initial position. */
    MutableBoard() {
        super();
        removed = new ArrayList<Piece>();
    }

    /** A Board whose initial contents and state are copied from BOARD. */
    MutableBoard(Board board) {
        super(board);
        removed = new ArrayList<Piece>();
    }

    /** Assuming isLegal(MOVE), make MOVE. */
    void makeMove(Move move) {
        if (this.isLegal(move)) {
            this.moves.add(move);
            Piece original = this.get(move.getCol0(), move.getRow0());
            Piece target = this.get(move.getCol1(), move.getRow1());
            removed.add(target);
            this.data[move.getRow1() - 1][move.getCol1() - 1] = original;
            this.data[move.getRow0() - 1][move.getCol0() - 1] = Piece.EMP;
            this._side = _side.opponent();
        } else {
            System.out.printf(this.toString());
            throw new LoaException("Move: " + move.toString()
                    + " is invalid!");
        }
    }

    /** Retract (unmake) one move, returning to the state immediately before
     * that move. Requires that movesMade () > 0. */
    void retract() {
        assert movesMade() > 0;
        Move lastmove = this.moves.remove(this.moves.size() - 1);
        Piece target = this.get(lastmove.getCol1(), lastmove.getRow1());
        this.data[lastmove.getRow0() - 1][lastmove.getCol0() - 1] = target;
        this.data[lastmove.getRow1() - 1][lastmove.getCol1() - 1] =
                removed.remove(removed.size() - 1);
        this._side = _side.opponent();
    }

    /** A list of all removed pieces. */
    private List<Piece> removed;

}
