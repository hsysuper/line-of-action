
package loa;

import ucb.junit.textui;
import org.junit.Test;

/** The suite of all JUnit tests for the loa package.
 *  @author Siyuan He
 */
public class UnitTest {

    /** Run the JUnit tests in the loa package. */
    public static void main(String[] ignored) {
        textui.runClasses(UnitTest.class);
        textui.runClasses(MoveTest.class);
        textui.runClasses(BoardTest.class);
    }

    /** A dummy test to avoid complaint. */
    @Test
    public void placeholderTest() {
    }

}


