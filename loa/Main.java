
package loa;

import ucb.util.CommandArgs;
import static loa.Side.*;

/** Main class of the Lines of Action program.
 * @author Siyuan He
 */
public class Main {

    /** Default for black and white. */
    private static final boolean DWHITE = false;

    /** Default for display toggle. */
    private static final boolean DDISPLAY = false;

    /** Default number of AIs. */
    private static final int DAI = 1;

    /** Default debug toggle. */
    private static final int DDEBUG = 0;

    /** Default seed value. */
    private static final long DSEED = 42;

    /** Default time limit. */
    private static final int DTIME = 60;

    /** Conversion parameter between minutes and seconds. */
    private static final double MS = 60;

    /** The main Lines of Action.  ARGS are as described in the
     *  project 3 handout:
     *      [ --white ] [ --ai=N ] [ --seed=S ] [ --time=LIM ] \
     *      [ --debug=D ] [ --display ]
     */
    public static void main(String... args) {
        boolean white = DWHITE;
        boolean display = DDISPLAY;
        int ai = DAI;
        int debug = DDEBUG;
        long seed = DSEED;
        int time = DTIME;
        CommandArgs options =
            new CommandArgs("--white --ai= "
                    + "--seed= --time= --debug= --display", args);
        if (!options.ok()) {
            System.err.println("Error: Args wrong!");
            usage();
        }
        white = options.containsKey("--white");
        display = options.containsKey("--display");
        try {
            if (options.containsKey("--ai")) {
                ai = Integer.parseInt(options.get("--ai").get(0));
                if (ai > 2 || ai < 0) {
                    usage();
                }
            }
            if (options.containsKey("--seed")) {
                seed = Long.parseLong(options.get("--seed").get(0));
                if (seed < 0) {
                    usage();
                }
            }
            if (options.containsKey("--time")) {
                time =
                        (int) (MS * Double.parseDouble(options.get("--time")
                                .get(0)));
                if (time <= 0) {
                    usage();
                }
            }
            if (options.containsKey("--debug")) {
                debug = Integer.parseInt(options.get("--debug").get(0));
                if (debug < 0) {
                    usage();
                }
            }
        } catch (NumberFormatException e) {
            System.err.println("Error: Please enter valid numbers!");
            usage();
        }
        Side side = white ? WHITE : BLACK;
        int hum = 2 - ai;
        Reporter.setMessageLevel(debug);
        Game game = new Game(hum, side, seed, time, display);
        game.play();
        System.exit(0);
    }


    /** Print brief description of the command-line format. */
    static void usage() {
        System.err.println("Usage: java loa.Main [ --white ] [ --ai=N ] "
                + "[ --seed=S ] [ --time=LIM ] [ --debug=D ] [ --display ]");
        System.exit(1);
    }
}
