
package loa;

/** Indicates a piece or player color.
 * @author Siyuan He */
enum Side {
    /** The names of the two sides and both sides. */
    BLACK, WHITE, BOTH;

    /** Return the opposing color. */
    Side opponent() {
        return this == BLACK ? WHITE : BLACK;
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
}
