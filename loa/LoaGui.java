package loa;

import java.awt.Color;
import java.nio.CharBuffer;
import java.util.ArrayList;

import ucb.gui.*;
import static loa.Side.*;

/** A class that build the Window for Line of Action. 
 *
 * @author Siyuan He
 *
 */
class LoaGui extends TopLevel{

    LoaGui(String title, boolean exitOnClose, Game game) {
        super(title, exitOnClose);
        _game = game;
        this.setMinimumSize(800, 800);
        this.setPreferredSize(800, 800);
        this.setMaximumSize(1440, 900);
        this.setUpMenu();
        this.buildBoard();
        super.display(true);
        _movearray = new ArrayList<int[]>();
    }

    void makeMove(Move move) {
        Piece2D origin = this.getPiece2D(move.getCol0(), move.getRow0());
        Piece2D target = this.getPiece2D(move.getCol1(), move.getRow1());
        target.setSide(origin.getSide());
        origin.setSide(null);
    }

    Piece2D getPiece2D(int c, int r) {
        return pieces[r - 1][c - 1];
    }

    void setUpMenu() {
        super.addMenuButton("Game->Play AI", "sendMenu");
        super.addMenuButton("Game->Quit", "sendMenu");
    }

    void sendMenu(String c) {
        if (c.equals("Game->Play AI")) {
            this.reportCommand("p");
        } else if (c.equals("Game->Quit")) {
            this.reportCommand("q");
        }
    }

    void buildBoard() {
        LayoutSpec top = new LayoutSpec("width", "rest", "height", 1, "x", 0, "y", 0, "fill", "both");
        super.addLabel("", top);
        pieces = new Piece2D[8][8];
        for (int i = 0; i < 8; i++) {
            super.addLabel("", new LayoutSpec("width", 1, "height", 1, "x", 0, "y", i));
            for (int j = 0; j < 8; j++) {
                Color bcolor = ((i + j) % 2 == 0) ? Color.LIGHT_GRAY : Color.GRAY;
                Side side;
                if ((i == 0 || i == 7) && (j > 0 && j < 7)) {
                    side = BLACK;
                } else if ((j == 0 || j == 7) && (i > 0 && i < 7)) {
                    side = WHITE;
                } else {
                    side = null;
                }
                pieces[i][j] = new Piece2D(this.getWidth() / 9, bcolor, side, this, j + 1, i + 1);
                super.add(pieces[i][j], new LayoutSpec("width", 1, "height", 1, "x", j + 1, "y", i + 1));
            }
            super.addLabel("", new LayoutSpec("width", 1, "height", 1, "x", 8, "y", i));
        }
        LayoutSpec bottom = new LayoutSpec("width", "rest", "height", 1, "x", 8, "y", 0, "fill", "both");
        super.addLabel("", bottom);
    }

    Move recordMove(Piece2D piece) {
        _movearray.add(new int[]{piece.getCol(), piece.getRow()});
        if (_movearray.size() >= 2) {
            Move move = Move.create(_movearray.get(0)[0], _movearray.get(0)[1], _movearray.get(1)[0], _movearray.get(1)[1]);
            _movearray.clear();
            return move;
        }
        return null;
    }

    void reportCommand(String command) {
        this._game.writeToBuffer(command + "\n");
    }

    private ArrayList<int[]> _movearray;
    private Piece2D[][] pieces;
    private Game _game;
}
