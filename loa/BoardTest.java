
package loa;

import static org.junit.Assert.*;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import static loa.Piece.*;
import static loa.Side.*;

/**A series of test for the game board, especially for
 * Board.java, MutableBoard.java
 *
 * @author Siyuan He
 *
 */
public class BoardTest {

    /**
     * A test on the isLegal method in Board.java.
     */
    @Test
    public void legalTest() {
        Board board = new Board();
        Move move1 = Move.create(2, 1, 4, 3);
        Move move2 = Move.create(2, 1, 6, 3);
        Move move3 = Move.create(1, 4, 5, 1);
        Move move4 = Move.create(6, 1, 1, 1);
        assertEquals("Move " + move1.toString() + " should be legal", true,
                board.isLegal(move1));
        assertEquals("Move " + move2.toString() + " should be illegal",
                false, board.isLegal(move2));
        assertEquals("Move " + move3.toString() + " should be illegal",
                false, board.isLegal(move3));
        assertEquals("Move " + move4.toString() + " should be illegal",
                false, board.isLegal(move4));
    }

    /**
     * Another test on the isLegal method in Board.java.
     */
    @Test
    public void legalTest2() {
        MutableBoard board = new MutableBoard();
        Move move1 = Move.create(2, 1, 4, 3);
        Move move2 = Move.create(1, 3, 4, 3);
        assertEquals("Move1 should be legal", true, board.isLegal(move1));
        board.makeMove(move1);
        assertEquals("Move2 should be legal", true, board.isLegal(move2));
    }

    /**
     * A test on the toString method in Board.java.
     */
    @Test
    public void printTesT() {
        Board board = new Board();
        System.out.printf(board.toString());
    }

    /**
     * A test on whether the board can detect contigeous pieces.
     */
    @Test
    public void countTest() {
        Board board = new Board();
        List<Point> list =
                board.countPiece(2, 1, BLACK, new ArrayList<Point>());
        for (Point i : list) {
            System.out.println(i.toString());
        }
    }

    /**
     * A test on whether the board could find the line of action.
     */
    @Test
    public void lineTest() {
        MutableBoard board = new MutableBoard();
        Move move1 = Move.create(2, 1, 4, 3);
        Move move2 = Move.create(1, 3, 4, 3);
        assertEquals("Move1 should be legal", true, board.isLegal(move1));
        board.makeMove(move1);
        assertEquals("Move2 should be in range", true, board.inRange(move2));
        List<Piece> line = board.getLoa(move2);
        for (Piece item : line) {
            System.out.println(item.textName());
        }
    }

    /**
     * Another test on whether the board can detect a winning move.
     */
    @Test
    public void countTest2() {
        MutableBoard board = new MutableBoard(TEST_PIECES, WHITE);
        System.out.printf(board.toString());
        Move move = Move.create(2, 6, 3, 7);
        board.makeMove(move);
        System.out.printf(board.toString());
        assertEquals("The game should be over!.", WHITE, board.gameOver());
    }

    /** Test the legalMovesFrom() method in Board. */
    @Test
    public void legalMovesTest() {
        MutableBoard board = new MutableBoard(TEST_PIECES2, BLACK);
        List<Move> moves = board.legalMovesFrom(6, 3, BLACK);
        System.out.printf(board.toString());
        System.out.println("Number of moves from f3: " + moves.size());
        for (Move move : moves) {
            System.out.println(move);
        }
    }

    /** Test if board can succesffully clone each other. */
    @Test
    public void cloneTest() {
        MutableBoard board1 = new MutableBoard(TEST_PIECES2, BLACK);
        System.out.printf(board1.toString());
        MutableBoard board2 = new MutableBoard(board1);
        assertEquals("Board2 is not properly initiated.", true, board2 != null);
        System.out.printf(board2.toString());
    }

    /** A test initial configuration for Lines of Action. */
    static final Piece[][] TEST_PIECES = {
        { EMP, EMP, EMP, EMP, EMP, EMP, BP, EMP },
        { EMP, EMP, EMP, EMP, EMP, EMP, EMP, EMP },
        { EMP, EMP, EMP, BP, EMP, BP, EMP, EMP },
        { EMP, BP, EMP, BP, EMP, EMP, EMP, EMP },
        { EMP, EMP, BP, BP, EMP, EMP, EMP, EMP },
        { EMP, WP, EMP, WP, EMP, EMP, EMP, BP },
        { EMP, WP, EMP, EMP, EMP, EMP, EMP, EMP },
        { EMP, EMP, EMP, EMP, EMP, EMP, BP, EMP } };

    /** A test initial configuration for Lines of Action. */
    static final Piece[][] TEST_PIECES2 = {
        { EMP, BP, EMP, BP, BP, EMP, EMP, EMP },
        { WP, EMP, EMP, EMP, EMP, EMP, EMP, WP },
        { WP, EMP, EMP, EMP, BP, BP, EMP, WP },
        { WP, EMP, BP, EMP, EMP, WP, EMP, EMP },
        { WP, EMP, WP, WP, EMP, WP, EMP, EMP },
        { WP, EMP, EMP, EMP, BP, EMP, EMP, WP },
        { EMP, EMP, EMP, EMP, EMP, EMP, EMP, EMP },
        { EMP, BP, BP, BP, EMP, BP, BP, EMP } };

}
